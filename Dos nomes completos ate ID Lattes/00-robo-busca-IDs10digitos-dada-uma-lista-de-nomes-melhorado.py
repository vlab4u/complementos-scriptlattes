#!/usr/bin/python
# encoding: utf-8
#
# Este programa permite buscar os IDs de 10 digitos (comecam com a letra K)
# de uma lista de nomes completos de pessoas.
#
# Exemplo: 
#          $ python 00-robo-busca-IDs10digitos-dada-uma-lista-de-nomes.py  lista-nomes.txt 
#
# A saída será da seguinte forma:
#            1 , Roberto Marcondes Cesar Junior , K4799759H4 , Roberto_Marcondes_Cesar_Junior
#            2 , Jesus Pascual Mena Chalco , K4500409Y4 , Jess_Pascual_Mena_Chalco
#            3 , Andrea Britto Mattos , K4468283H5 , Andra_Britto_Mattos
#
# - A segunda coluna corresponde ao nome dado no arquivo (nome a procurar)
# - A terceira coluna é o código de 10 dígitos.
# - A quarta coluna é o nome identificado na Plataforma Lattes.
#
#
#
# Foi criado um outro script que permite converter os codigos de 10 digitos para
# os codigos de 16 digitos (os codigos utilizados pelo scriptLattes).
# Veja o arquivo: 02-converter-codigos-lattes-de-10-a-16-digitos.py
# 
#
# Jesús P. Mena-Chalco <jesus.mena@ufabc.edu.br>
# 
# Qua Jan 22 20:04:37 BRST 2014
#

import sys
import shutil
import os, errno
import fileinput
import numpy
import time
import urllib2
import re
import unicodedata


def remove_accents(input_str):
	nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
	return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	inFile = sys.argv[1]
	http = "http://buscatextual.cnpq.br/buscatextual/busca.do?"
	httpVariaveis = "&metodo=buscar&buscaAvancada=0&filtros.buscaNome=true&buscarDoutores=true&buscarDemais=true&buscarBrasileiros=true&buscarEstrangeiros=true&paisNascimento=0&buscarDoutoresAvancada=true&buscarBrasileirosAvancada=true&buscarEstrangeirosAvancada=true&paisNascimentoAvancada=0&filtros.atualizacaoCurriculo=48&quantidadeRegistros=10"
	i = 1
	output = ""

	for line in fileinput.input(inFile):
		line = line.strip()
		line = line.decode('utf-8', 'replace')
		line  = remove_accents(line)

		nomeCompleto = line
		nomeCompleto = nomeCompleto.replace(" ","%20")
		textoBusca = "textoBusca="+nomeCompleto
		url = http + textoBusca + httpVariaveis

		tentativa = 0
		while tentativa<=5:
			try:
				# Para cada pagina (contendo o nome de UMA pessoa)
				req       = urllib2.Request(url)
				arquivoH  = urllib2.urlopen(req)
				pagina    = arquivoH.read()
				arquivoH.close()
				time.sleep(1)

				if len(pagina)<=2000:
					print "\n[AVISO] Tentaremos baixar novamente o seguinte CV Lattes: ", url
					time.sleep(30)
					tentativa+=1
					continue
				break
			except:
				print "\n[Procurando",tentativa,"]",nomeCompleto,"Nao é possível fazer a busca: ", url
				tentativa+=1
				continue

		regex           = re.compile(r"javascript:abreDetalhe(.*)\"")
		CVsPessoas      = regex.findall(pagina)
		lista10Pessoas  = set(CVsPessoas)
		
		if len(lista10Pessoas)==0:
			output += str(i)+", "+ line +" , NAO-IDENTIFICADO" + "\n"

		item = 0
		for detalhe in lista10Pessoas:
			detalhe = detalhe.replace("'","")
			detalhe = detalhe.replace("(","")
			detalhe = detalhe.replace(")","")
			det = detalhe.split(',')
			endereco = det[0]
			nome     = det[1]
			if item==0:
				item +=1
				output += str(i)+", "+ line +" , "+ endereco +" , "+ nome.decode('utf-8','ignore') + "\n"
			else:
				output += "--"+", "+ line +" , "+ endereco +" , "+ nome.decode('utf-8','ignore') + "\n"
		i +=1


	file = open(inFile+'.saida.txt', 'w')
	file.write(output.encode('utf8','replace'))
	file.close()

	
