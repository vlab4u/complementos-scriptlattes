#!/usr/bin/python
# encoding: utf-8
#

import sys
import shutil
import os, errno
import fileinput


# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	fAutores       = sys.argv[1]
	fColaboradores = sys.argv[2]

	# ---------------------------------------------------------------------------- #
	autores   = []
	autoresID = []
	for linha in fileinput.input(fAutores):
		linha = linha.replace("\r","")
		linha = linha.replace("\n","")
		nID   = linha.split(",")[0].strip()

		autores.append(linha.strip())
		autoresID.append(nID)

	# ---------------------------------------------------------------------------- #
	# colaboradores únicos na lista de colaboradores
	colaboradores   = []
	for linha in fileinput.input(fColaboradores):
		linha = linha.replace("\r","")
		linha = linha.replace("\n","")

		colaboradores.append(linha.strip())

	print "- colaboradores totais :" + str(len(colaboradores))
	colaboradores = list(set(colaboradores))
	print "- colaboradores unicos :" + str(len(colaboradores))

	colaboradoresID = []
	for col in colaboradores:
		colID = col.split(",")[0].strip()
		colaboradoresID.append(colID)
		
	# ---------------------------------------------------------------------------- #
	# alguns colaboradores da lista unica, pode também estar na lsita de autores
	for i in  range(0,len(autoresID)):
		autID =  autoresID[i]
		for j in range(0,len(colaboradoresID)):
			colID = colaboradoresID[j]
			if autID == colID:
				colaboradoresID[j] = ""

	# ---------------------------------------------------------------------------- #
	reaisColaboradores = []
	for j in range(0,len(colaboradoresID)):
		if (not colaboradoresID[j]==""):
			autores.append(colaboradores[j] + " , , , Colaborador-Lattes")

	# ---------------------------------------------------------------------------- #
	fOutput = open(fColaboradores[:-4] + "-e-autores.txt", "w")
	
	for aut in autores:
		fOutput.write(aut+"\n")




