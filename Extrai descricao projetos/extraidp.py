﻿# Programa: extraidp.py
# Versão: 2013.10.25
#
# Função: Extrai a lista de títulos e descrições de projetos da página gerada pelo scriptlattes. 
# 
# (temporariamente desabilitada )Sintaxe: extraidp.py <url da página> <arquivo saida>
# Veja configurações 
# Programador: André Santos

import fileinput
import re
import sys
import shutil
import unicodedata
import StringIO
import urllib2
from lxml import etree
from lxml.html import fromstring, tostring

#funções desenvolvidas por Jesus Menna
def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
    if unicodedata.category(c) != 'Mn')

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #

if __name__ == "__main__":
#    url = sys.argv[1]
#    oufile = sys.argv[2]
# --------------------------------------------------------------------------------- #
# Configurações
# --------------------------------------------------------------------------------- #
    oufile = 'vamos.txt' # Nome do arquivo de saida
    infile = '' #Nome do arquivo em cache, se for utilizar cache. 
    url = 'http://localhost/vlab4u/doencas%20negligenciadas/Dengue/denguecore1/denguecore1results/Pj-0.html'
    source = 1 #Usar 1 se for extrair de uma url ou 0 para extrair de página salva em cache
    if source == 1:
        request = urllib2.Request(url)
        rawPage = urllib2.urlopen(request)
        read = rawPage.read()
    else:
        
        with open('projetos.html', 'r') as fo:
            read = fo.read()
    
    tree = etree.HTML(read)
    #antigo = '//tr[*]/td[2]/b|//tr[*]/td[2]/i[1]/font'
    projetos = tree.xpath('//table[*]/tr[*]/td[2]/b|//table[*]/tr[*]/td[2]/i[1]/font|//table[*]/tr[*]/td[2]/span')
    if len(projetos) > 0:
        linha=''  
        for i in range(0, len(projetos)-2, 3):
            titulo      = ''
            resumo      = ''
            ano         = ''
            ano         = remove_accents(projetos[i].text)
            titulo      = remove_accents(projetos[i+1].text)
            resumo      = remove_accents(projetos[i+2].text)
            descricao   = ''
            natureza    = 'Outra'
            financiador = ''
            
            if re.search('Situacao: Em andamento', resumo):
               situacao = 'Em andamento'
            if re.search('Situacao: Concluido', resumo):
               situacao = 'Concluido'
            if re.search('(?<=Descricao:\s).*(?=\..?\..?Situacao:)', resumo):
                    descricao   = re.search('(?<=Descricao:\s).*(?=\..?\..?Situacao:)', resumo).group(0)
            if re.search('(?<=Natureza: Desenvolvimento).*', resumo):
                    natureza = 'Desenvolvimento'
            if re.search('(?<=Natureza: Pesquisa).*', resumo):
                    natureza = 'Pesquisa'
            if re.search('(?<=Financiador\(es\):\s).*(?=\-)', resumo):
                    financiador = re.search('(?<=Financiador\(es\):\s).*(?=\-)', resumo).group(0)
                    financiador = financiador.replace('/',';')
            
            linha = linha + ano + '\t' + situacao + '\t' + titulo + '\t' + natureza + '\t' + financiador + '\t' + descricao + '\n'
        texto=linha.encode('utf8')
        outputfile = open(oufile, 'w')
        outputfile.write(texto)
        outputfile.close()
