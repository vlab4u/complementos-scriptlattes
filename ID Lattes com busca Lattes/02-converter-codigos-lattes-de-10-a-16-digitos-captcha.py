#!/usr/bin/python
# encoding: utf-8
#
# Jesús P. Mena-Chalco <jesus.mena@ufabc.edu.br>
#

import sys
import shutil
import os, errno
import fileinput
import urllib2
import time, datetime, os


# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	inFile = sys.argv[1]
	http   = "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?metodo=captchaValido&id="
	s      = ""

	now = datetime.datetime.now()
	newDirName = now.strftime("%Y_%m_%d-%H%M")
	print "Diretorio cache para armazenar os CVs Lattes baixados: " + newDirName
	os.mkdir(newDirName)

	
	for line in fileinput.input(inFile):
		line = line.strip()
		id10 = line.split(',')
		id10 = id10[0].strip()
		
		if len(id10)>0:
			url = http + id10
			# print url
			
			contador = 1
			while contador<=20:	
				try:
					req = urllib2.Request(url)
					arquivoH = urllib2.urlopen(req)
					cvLattesHTML = arquivoH.read()
					arquivoH.close()
					time.sleep(1)
				except:
					cvLattesHTML = ""
				
				if len(cvLattesHTML)>=8500:
					break
				print "Tentando uma vez mais..."
				
				time.sleep(10)
				contador +=1
			
			#print len(cvLattesHTML)
			texto = cvLattesHTML.split("\n")	
			for linha in texto:
				if "o para acessar este CV:" in linha:
					partes = linha.split("http://lattes.cnpq.br/")
					id16 = partes[1].strip("</li>")
					t = id10 +","+  id16
					s += t+"\n"
					print t
					
					cvPath = newDirName +"/"+ id16
					file = open(cvPath, 'w')
					file.write(cvLattesHTML)
					file.close()

	file = open(inFile+".IDs16digitos.csv", 'w')
	file.write(s)
	file.close()
	
