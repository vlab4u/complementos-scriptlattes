﻿# Seg Abr  1 19:09:00 BRT 2013

Procedimentos para baixar automaticamente os CVs Lattes, considerando uma busca
no sistema: http://buscatextual.cnpq.br/buscatextual/busca.do


PARTE 1:
========

- Para identificar os CVs Lattes dada una busca inicial, primeiro deve se utilizar
  o browser e indicar os parametros de busca.
  Click na SEGUNDA pagina, i.e., pagina 2.
  Vera que na URL aparece uma URL com varios parametros

- Depois deve-se copiar a URL e atualizar as variaveis: 
  -> urlBase   : URL anterior
  -> numFim    : variavel que contem o numero total de CVs identificados na busca
  do script 01-robo-baixa-codigos-lattes-de-10-digitos.py
  Salve o arquivo e depois execute-o.

- Executar o script seguindo o seguinte formato:
  $ ./01-robo-baixa-codigos-lattes-de-10-digitos.py > doencas_negligenciadas-IDs10.txt
  O script carregara toda pagina e idnetificara os codigos lattes de 10 digitos.

  Caso não funcione a instrução anterior, pode executar a seguinte instrução:
  $ python  01-robo-baixa-codigos-lattes-de-10-digitos.py > doencas_negligenciadas-IDs10.txt

  

PARTE 2:
========

Para converter os codigos de 10 digitos
  $ ./02-converter-codigos-lattes-de-10-a-16-digitos-captcha.py doencas_negligenciadas-IDs10.txt  > doencas_negligenciadas-IDs16.txt

A plataforma Lattes esta usando o teste captcha e por isso eh necessario usar esta nova versao.

Pronto.
======

Esses IDs Lattes de 16 digitos podem ser utilizados no scriptLattes


